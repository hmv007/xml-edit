const fs = require('fs');
var xml2js = require('xml2js');

let xmlString = fs.readFileSync('./ext.xml', 'utf-8');

xml2js.parseString(xmlString, (err, res)=>{
    delete res['wsdl:definitions']['ifw:properties'];
    const xmlBuilder = new xml2js.Builder();
    const finalXML = xmlBuilder.buildObject(res);

    fs.writeFileSync('./tagRemoved2.xml', finalXML);    
});